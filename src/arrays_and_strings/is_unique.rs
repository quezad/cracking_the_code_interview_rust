pub fn all_chars_unique_part_a(string: &str) -> bool{
    use std::collections::HashSet;
    let mut characters: HashSet<char> = HashSet::new();

    for character in string.chars(){
        if characters.contains(&character){
            return false;
        }
        characters.insert(character);
    }
    true
}

pub fn all_chars_unique_part_b(string: &str) -> bool{
    let mut bitfield: i64 = 0;
    let a_int_char: i16 = 'a' as i16;

    for character in string.chars(){
        let mut int_char: i16 = character as i16;
        int_char -= a_int_char;

        if (1 << int_char) & bitfield != 0{
            return false;
        }

        // set bit
        bitfield |= 1 << int_char;
    }

    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_a(){
        assert_eq!(all_chars_unique_part_a(&String::from("abcdefg")), true);
        assert_eq!(all_chars_unique_part_a(&String::from("abcdefga")), false);
    }

    #[test]
    fn test_part_b(){
        assert_eq!(all_chars_unique_part_b(&String::from("abcdefg")), true);
        assert_eq!(all_chars_unique_part_b(&String::from("abcdefga")), false);
    }
}


