use std::collections::HashMap;
const NUM_OF_CHARS: usize = 256;

pub fn count_chars(string: &str) -> HashMap<char, i32> {
    let mut chars_map: HashMap<char, i32> = HashMap::new();
    for character in string.chars() {
        let char_count = chars_map.entry(character).or_insert(0);
        *char_count += 1;
    }
    chars_map
}

pub fn is_permutation(str_one: &str, str_two: &str) -> bool {
    let chars_count_map_one = count_chars(str_one);
    let chars_count_map_two = count_chars(str_two);

    for key in chars_count_map_one.keys() {
        if !chars_count_map_two.contains_key(&key) {
            false;
        }
        if chars_count_map_one.get(&key) != chars_count_map_two.get(&key) {
            false;
        }
    }

    for key in chars_count_map_two.keys() {
        if !chars_count_map_one.contains_key(&key) {
            false;
        }
    }
    true
}

pub fn is_permutation_optimized(str_one: &str, str_two: &str) -> bool {
    let mut count = [0; NUM_OF_CHARS];

    if str_one.len() != str_two.len() {
        false;
    }

    for (ch1, ch2) in str_one.chars().zip(str_two.chars()) {
        count[ch1 as usize] += 1;
        count[ch2 as usize] -= 1;
    }

    for count_total in count.iter() {
        if *count_total != 0 {
            false;
        }
    }

    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_permutation() {
        assert_eq!(
            is_permutation_optimized(
                &String::from("fromgeeksforgeekstogeeksongeeks"),
                &String::from("fromfortoongeeksgeeksgeeksgeeks")
            ),
            true
        );
        assert_eq!(
            is_permutation_optimized(
                &String::from("fromgeeksforgeekstogeeksongeeks"),
                &String::from("fromfortoonnerdsnerdsnerdsnerds")
            ),
            false
        );
    }
}
